from django.http import HttpResponse
import datetime

from django.shortcuts import render_to_response, render


def vindecoder (request):
    context = {}
    return render(request, 'vindecoder.html', context)
